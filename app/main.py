import telebot
from decouple import config
import re
from modules.functions import Screenshot, create_or_clear_folder, send_messages

bot = telebot.TeleBot(config('API_TOKEN'))
create_or_clear_folder()
send_messages = send_messages()

#Start chatting with bot 
@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, send_messages['start'])

@bot.message_handler(content_types=['text'])
def resolve(message):
    
    if message.text.find('http') != -1 :
        bot.send_message(message.chat.id, send_messages['process'])
        screen = Screenshot(message.text)
        filename = screen.take_capture()
        filename_url = open(f"screenshots/{filename}", 'rb')
        bot.send_photo(message.chat.id, filename_url)
    else:
        bot.send_message(message.chat.id, send_messages['wrong'])

bot.polling()
