import time
import os
import json
import shutil
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

class Screenshot:
    def __init__(self, url):
        self.url = url

    def take_capture(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--start-maximized')
        driver = webdriver.Chrome(ChromeDriverManager().install())
        driver.get(self.url)
        time.sleep(2)

        #the element with longest height on page
        height = driver.find_element_by_tag_name('body')
        body_height = height.size['height']
        driver.set_window_size(1920, body_height)      #the trick
        time.sleep(2)
        urlnameslash = self.url.split("/")[2]
        urlname = urlnameslash.split(".")[0]
        driver.save_screenshot("screenshots/{}.png".format(urlname))
        driver.quit()
        return '{}.png'.format(urlname)


def create_or_clear_folder():
    try:
        folder = 'screenshots'
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print(e)
    except:
        os.mkdir('screenshots')

def send_messages():
    with open('app/modules/messages_to_send.json', encoding='utf8') as f:
        messages = json.load(f)
        return messages